<?php

/**
 *
 * @link              http://elvismdev.io/
 * @since             1.0.0
 * @package           Gc_Functionalities
 *
 * @wordpress-plugin
 * Plugin Name:       GC Functionalities
 * Plugin URI:        https://bitbucket.org/grantcardone/gc-functionalities
 * Description:       DO NOT DEACTIVATE THIS PLUGIN. It contains code needed for custom functionalities on this website.
 * Version:           1.0.0
 * Author:            Elvis Morales
 * Author URI:        http://elvismdev.io/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       gc-functionalities
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-gc-functionalities-activator.php
 */
function activate_gc_functionalities() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-gc-functionalities-activator.php';
	Gc_Functionalities_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-gc-functionalities-deactivator.php
 */
function deactivate_gc_functionalities() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-gc-functionalities-deactivator.php';
	Gc_Functionalities_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_gc_functionalities' );
register_deactivation_hook( __FILE__, 'deactivate_gc_functionalities' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-gc-functionalities.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_gc_functionalities() {

	$plugin = new Gc_Functionalities();
	$plugin->run();

}
run_gc_functionalities();
