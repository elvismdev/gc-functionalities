<?php

/**
 * Fired during plugin activation
 *
 * @link       http://elvismdev.io/
 * @since      1.0.0
 *
 * @package    Gc_Functionalities
 * @subpackage Gc_Functionalities/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Gc_Functionalities
 * @subpackage Gc_Functionalities/includes
 * @author     Elvis Morales <elvis@grantcardone.com>
 */
class Gc_Functionalities_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
