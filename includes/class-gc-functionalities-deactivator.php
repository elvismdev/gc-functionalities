<?php

/**
 * Fired during plugin deactivation
 *
 * @link       http://elvismdev.io/
 * @since      1.0.0
 *
 * @package    Gc_Functionalities
 * @subpackage Gc_Functionalities/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Gc_Functionalities
 * @subpackage Gc_Functionalities/includes
 * @author     Elvis Morales <elvis@grantcardone.com>
 */
class Gc_Functionalities_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
