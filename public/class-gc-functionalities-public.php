<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       http://elvismdev.io/
 * @since      1.0.0
 *
 * @package    Gc_Functionalities
 * @subpackage Gc_Functionalities/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Gc_Functionalities
 * @subpackage Gc_Functionalities/public
 * @author     Elvis Morales <elvis@grantcardone.com>
 */
class Gc_Functionalities_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Gc_Functionalities_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Gc_Functionalities_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/gc-functionalities-public.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Gc_Functionalities_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Gc_Functionalities_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/gc-functionalities-public.js', array( 'jquery' ), $this->version, false );

	}

	/**
	 * Change twitter message for Motivational Stickers products only.
	 *
	 * @since    1.0.0
	 */
	public function gcf_woocommerce_sc_twitter_message( $message, $order, $product ) {

		if ( $product->id == 57625	// Success is My Duty Motivational Sticker
			|| $product->id == 57623	// Obsessed Motivational Sticker
			|| $product->id == 57618	// No Negativity Motivational Stickers
			|| $product->id == 57616	// 10XERS Motivational Sticker
			|| $product->id == 57614	// Young Hustlers Motivational Sticker
			|| $product->id == 57613	// Who’s Got My Money Motivational Stickers
			|| $product->id == 57607	// Sell or Be Sold Motivational Sticker
			|| $product->id == 57605	// 10X Family Motivational Sticker
			) {
			return 'Hey I just got this motivational sticker from Grant Cardone!';
		}

		return $message;

	}

}
